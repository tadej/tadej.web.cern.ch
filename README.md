# Tadej Novak's CERN Web Page

[![Homepage][web-img]][web]
[![License][license-img]][license]

[web]: https://tadej.web.cern.ch
[license]: https://gitlab.cern.ch/tadej/tadej.web.cern.ch/blob/staging/LICENSE.md
[web-img]: https://img.shields.io/badge/web-tadej.web.cern.ch-green.svg
[license-img]: https://img.shields.io/badge/license-CC%20BY--SA%204.0-blue.svg
