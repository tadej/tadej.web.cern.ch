import { Footer as FooterBase, FooterLink } from '@ntadej/style'

export default function Footer() {
  return (
    <FooterBase homeUrl="https://tadej.web.cern.ch">
      <FooterLink href="https://cern.ch/particle-clicker/" target="_blank">
        Particle Clicker
      </FooterLink>
      <FooterLink href="https://tano.si" target="_blank">
        Personal Projects
      </FooterLink>
    </FooterBase>
  )
}
