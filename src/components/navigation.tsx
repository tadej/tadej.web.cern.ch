import { Navigation as NavigationCore, SocialLink } from '@ntadej/style'
import { ImAddressBook } from 'react-icons/im'
import { SiGithub, SiGitlab, SiLinkedin, SiMastodon } from 'react-icons/si'

const navigation = [
  { name: 'About', href: '/' },
  { name: 'Particle Clicker', href: '/particle-clicker' },
]

export function Navigation() {
  return <NavigationCore items={navigation} />
}

export function Socials() {
  return (
    <div className="mt-4 flex gap-6">
      <SocialLink
        name="Follow on Mastodon"
        href="https://mastodon.social/@ntadej"
        icon={SiMastodon}
      />
      <SocialLink
        name="Follow on GitHub"
        href="https://github.com/ntadej"
        icon={SiGithub}
      />
      <SocialLink
        name="Follow on CERN GitLab"
        href="https://gitlab.cern.ch/tadej"
        icon={SiGitlab}
      />
      <SocialLink
        name="Follow on LinkedIn"
        href="https://www.linkedin.com/in/ntadej"
        icon={SiLinkedin}
      />
      <SocialLink
        name="Contact"
        href="https://phonebook.cern.ch/search?q=Tadej+Novak"
        icon={ImAddressBook}
      />
    </div>
  )
}
