import { RootLayout, rootMetadata } from '@ntadej/style'
import { Metadata } from 'next'
import { ReactNode } from 'react'

import '@/style/main.css'

export default function PageLayout({ children }: { children: ReactNode }) {
  return <RootLayout>{children}</RootLayout>
}

export const metadata: Metadata = rootMetadata
