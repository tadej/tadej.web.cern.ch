import {
  Content,
  Hero,
  Link,
  LinkButton,
  RemoteLinkList,
  Section,
  siteName,
} from '@ntadej/style'
import { Metadata } from 'next'
import Image from 'next/image'
import particleClickerDetector from './detector.png'

export const metadata: Metadata = {
  title: `Particle Clicker - ${siteName}`,
}

export default function ParticleClicker() {
  const linksAuthors = {
    'Gabor Biro': 'https://github.com/gbiro',
    'Igor Babuschkin': 'https://github.com/ibab',
    'Kevin Dungs': 'https://github.com/kdungs',
    'Tadej Novak': 'https://github.com/ntadej',
    'Jiannan Zhang': 'https://github.com/zhangjiannan',
  }

  const linksMedia = {
    CERN: 'http://home.web.cern.ch/about/updates/2014/08/code-and-coffee-innovative-projects-cern-webfest',
    'Science Magazine': 'http://www.sciencemag.org/content/345/6199/856.summary',
    'Symmetry Magazine':
      'http://www.symmetrymagazine.org/article/august-2013/new-game-trades-clicks-for-physics-discoveries',
    'Popular Science':
      'http://www.popsci.com/article/gadgets/click-your-way-discovery-cerns-particle-clicker-game',
    'Libération (French)':
      'http://ecrans.liberation.fr/ecrans/2014/08/13/particle-clicker_1080078',
    'Boing Boing':
      'http://boingboing.net/2014/08/11/particle-clicker-meth-addicti.html',
    'Jay is games': 'http://jayisgames.com/archives/2014/08/particle_clicker.php',
  }

  const subtitle = (
    <>
      Particle Clicker is a simple but addictive incremental game, based on the idea of{' '}
      <Link href="http://orteil.dashnet.org/cookieclicker/" target="_blank">
        Cookie Clicker
      </Link>
      . It was created over a weekend during the{' '}
      <Link href="https://webfest.web.cern.ch" target="_blank">
        CERN Summer Student Webfest 2014
      </Link>
      .
    </>
  )

  return (
    <>
      <Hero>
        <div className="grid grid-cols-2 content-center">
          <div className="max-w-md">
            <Image src={particleClickerDetector} alt="Particle Clicker" />
          </div>
          <div className="flex max-w-md flex-col justify-center text-center">
            <h1 className="text-3xl font-bold text-brand-primary sm:text-5xl">
              Particle Clicker
            </h1>
            <p className="mt-4 text-pretty text-xl font-medium text-zinc-700 dark:text-zinc-300 sm:mt-6 sm:text-2xl">
              An addictive incremental game that teaches players the history of high
              energy particle physics
            </p>
            <div className="mt-8">
              <LinkButton href="http://cern.ch/particle-clicker" target="_blank">
                Play now!
              </LinkButton>
            </div>
          </div>
        </div>
      </Hero>

      <Section title="About" subtitle={subtitle}>
        <div className="container">
          <div className="grid gap-4 md:grid-cols-2">
            <Content fullWidth={true}>
              <h2>Authors</h2>
              <p>
                We are a group of (former) students from several different countries
                that met at CERN either before as Summer Students or at the CERN
                Webfest.
              </p>
              <RemoteLinkList links={linksAuthors} />
            </Content>

            <Content fullWidth={true}>
              <h2>Media response</h2>
              <RemoteLinkList links={linksMedia} />
            </Content>
          </div>
        </div>
      </Section>
    </>
  )
}
